# Vm-starter--lamp

## Introdution

Box build from [Scotchbox build scripts](https://github.com/scotch-io/scotch-box-build-scripts)

## Things installed

- Ubuntu 20.04
- PHP 7.2 (default) / 7.4 / 8.1
- composer 1
- MySQL 8.0
- NGINX Option
- Go lang in the box
- PHPUnit in the box
- MailHog : launch command "mailhog" after vagrant ssh
- nodejs
- npm
- dos2unix : 
- shellcheck : 


## Todo

- choice of php version
- configurable web directory
- Pre install usefull php tools (for debugging for exemple phpcs, phpstan)
- automate publication of boxe on vagrantup.com (via pipeline)

## Customize

- customise the scripts
- run ./shell/vendor/clean_box.sh 
- upload you change on vagrant with "vagrant cloud publish ..."

### Composer 2

 - vssh 
 - composer self-update --2

## FAQ

- How to change PHP version (exemple switch from 7.2 to 7.4) ?
```
sudo a2dismod php7.2
sudo a2enmod php7.4
sudo update-alternatives --set php /usr/bin/php7.4
sudo service apache2 restart
```


## Ressources
- [Scotchbox](https://github.com/scotch-io/scotch-box)
- [Scotchbox build scripts](https://github.com/scotch-io/scotch-box-build-scripts)